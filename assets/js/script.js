$(document).ready(function() {
    var globalPrice = 0

    $('.add-to-cart').on('click', 'i', function() {
        var oldVal = parseInt($(this).parent('.add-to-cart').children('input').val())
        
        if ($(this).hasClass('add')) {
            var newVal = oldVal + 1
        } else if ($(this).hasClass('remove')) {
            var newVal = oldVal - 1
        }

        $(this).parent('.add-to-cart').children('input').val(newVal)
        
        var price = $(this).closest('.buy').find('.total p.price')
        
        var newPrice = newVal * price.attr('value')
        globalPrice = newPrice
        price.text('R$ ' + newPrice + ',00')
    });

    $('.buy').on('click', 'button', function() {
        var oldTotal = $(this).closest('body').find('.cart-total')

        if (globalPrice == 0) {
            globalPrice = parseInt($(this).closest('.buy').find('p.price').attr('value'))
        }

        var newTotal = parseInt(oldTotal.text()) + globalPrice

        oldTotal.text(newTotal + ',00')
    });
});